# README #

Hound, the Robot.

### About Hound ###

I created Hound because i wanted to compete with my own robot.
With this in mind i bought an chassis, some sensors and an Arduino Uno.

### Objectives ###

* Line Follower
* Collision avoidance

### Future Work ###

* Build Control Center WebApp
* Integrate with Raspberry Py